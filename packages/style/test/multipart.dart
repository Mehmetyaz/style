import 'package:http/http.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() async {
test("description", () async {
  var req = MultipartRequest(
      "POST", Uri.parse("http://localhost:80/appointments/servicer1/1/create"));
  req.files
    ..add(await MultipartFile.fromPath(
        "a", "D:\\style\\packages\\style\\source\\a.html",
        filename: "a.html"))
  ..add(await MultipartFile.fromPath(
  "a", "D:\\style\\packages\\style\\source\\b.html",
  filename: "b.html"));

  var res = await req.send();

  print(res.headers);
  expect(res.statusCode, 400);
});
}
